END-TOEND PIPELINE STEPS FOR USER

Step1: 3D slicer
1 - on 3D slicer run PathPlanner module for the optimised path to be given
2 - From OpenIGTLinkIF setup OpenIGTLink by entering 1894 as port number
3 - in the Input Output option on the left, choose pahOptimised_transofmred as output

Step2: ROS:Building the bridge connection
In a new terminal:

$ cd ~/catkin_ws
$ source devel/setup.bash
$ roslaunch ros_igtl_bridge bridge launch
choose <2> as client 
ServerIP is obtained from network settings --> Wired --> Default Route
ServerPort as written on 3D slicer 18944

Step3: ROS: Loading Rviz
In a new terminal:

$ cd ~/catkin_ws
$ source devel/setup.bash
$roslaunch NeuroRobot_moveit demo.launch

Step4: Loading robot controller
In a new terminal

$ cd ~/catkin_ws
$ source devel/setup.bash
$ rosrun NeuroRobot_controller igtl_importer.py


